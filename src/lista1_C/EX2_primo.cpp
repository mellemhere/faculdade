#include <iostream>

/*

        FEITO POR GABRIEL MELLEM
        mellemhere.com
        10.04.2016

 */

int main(int argc, char** argv) {

    /*
     DEFINE AS VARIAVEIS QUE SERAO USADAS
     */
    int cont, x;

    //Loop principal, isso faz o programa ficar perguntando numeros novamente
    while (1) {
        //Variavel boleana que segura o resultado do for-loop
        bool r = true;

        //Imprime na tela para o usuario inserir um numero
        printf("Me de um n: ");
        //Le oque o usuario imprimiu
        scanf("%d", &x);

        if (x == 0) {
            continue;
        } else if (x == 1) {
            printf("0");
        }

        //Faz um loop que tenta dividir o numero que o usuario informou
        //por todos os numeros de 2 ate o numero que o usuario informou-1
        //Ex. o usuario inseriu 5, o loop ira executar ate que cont for 4
        for (cont = 2; cont != x; cont++) {

            //Divide o numero que o usuario informou por cont e verifica se
            //o resto do resultado 'e zero. Caso sim o numero nao 'e primo
            if ((x % cont) == 0) {
                //Avisa o usuario que o numero nao 'e primo
                printf("Nao 'e primo' \n");
                //Define o resultado como nao primo na variavel r
                r = false;
                //Para o loop
                break;
            }
        }

        //Caso o resultado (r) ainda seja true, quer dizer que o loop nao conseguiu
        //dividir por nenhum outro numero, entao, o numero 'e primo
        if (r) {
            printf("'e primo \n");
        }
    }
}
