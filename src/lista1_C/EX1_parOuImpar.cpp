#include <iostream>

/*

        FEITO POR GABRIEL MELLEM
        mellemhere.com
        10.04.2016

 */

int main(int argc, char** argv) {
    
    /*
     DEFINE AS VARIAVEIS QUE SERAO USADAS
    */
    int cont, x;
    
    //Loop principal, isso faz o programa ficar perguntando numeros novamente
    while (1) {
        //Variavel boleana que segura o resultado do for-loop
        bool r = true;
        
        //Imprime na tela para o usuario inserir um numero
        printf("Me de um n: ");
        //Le oque o usuario imprimiu
        scanf("%d", &x);
        
        /*
         Verifica se 'e possivel a divisao por 2
         */
        if((x % 2) == 0){
            printf("Numero par \n");
        }else{
            printf("Numero impar \n");
        }

    }
}
