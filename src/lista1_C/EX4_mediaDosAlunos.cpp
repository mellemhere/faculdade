#include <stdio.h>
#include <stdlib.h>

/*
 
    FEITO POR GABRIEL MELLEM
    mellemhere.com
    10.04.2016
 
 */


int main(int argc, char** argv) {
    int numeroDeAlunos = 10, numeroDeNotas = 4, cont, cont2;

    for (cont = 0; cont < numeroDeAlunos; cont++) {
        int somaNotas = 0, qNotas = 0;
        for (cont2 = 0; cont2 < numeroDeNotas; cont2++) {
            int nota = 0;
            printf("Qual a %d* nota?", cont2 + 1);
            scanf("%d", &nota);
            somaNotas += nota;
            qNotas++;
        }
        if ((somaNotas / qNotas) >= 7) {
            printf("Aprovado :) Media: %d \n", somaNotas / qNotas);
        } else {
            printf("Reprovado :(  Media: %d \n", somaNotas / qNotas);
        }
    }

}

