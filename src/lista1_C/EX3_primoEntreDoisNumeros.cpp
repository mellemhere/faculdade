#include <iostream>

/*

        FEITO POR GABRIEL MELLEM
        mellemhere.com
        12.04.2016

 */

bool testeSePrimo(int x) {
    /*
            Codigo que testa se numero 'e primo ou nao
     */


    int cont;

    //Variavel boleana que segura o resultado do for-loop
    bool r = true;

    if (x == 0 || x == 1) {
        return false;
    }

    //Faz um loop que tenta dividir o numero que o usuario informou
    //por todos os numeros de 2 ate o numero que o usuario informou-1
    //Ex. o usuario inseriu 5, o loop ira executar ate que cont for 4
    for (cont = 2; cont != x; cont++) {
        //Divide o numero que o usuario informou por cont e verifica se
        //o resto do resultado 'e zero. Caso sim o numero nao 'e primo
        if ((x % cont) == 0) {
            return false;
        }
    }
    return true;
}

int main(int argc, char** argv) {

    /*
        Loop principal para continuar rodando o programa
     */
    while (1) {
        //Declara as variaveis que seram usadas
        int primeiroNumero, segundoNumero, cont, resultado = 0;

        //Pede pelo primeiro numero e salva em primeiroNumero
        printf("Me de o primeiro numero: ");
        scanf("%d", &primeiroNumero);

        //Pede pelo segundo numero e salva em segundoNumero	
        printf("Me de o segundo numero: ");
        scanf("%d", &segundoNumero);

        /*
            Faz um loop entre os numeros dados verificando cada um se 'e primo
         */
        for (cont = primeiroNumero; cont != segundoNumero; cont++) {
            if (testeSePrimo(cont)) {
                //Caso seja primo incrementa na int resultado
                resultado++;
            }
        }
        
        
        //Imprime o resultados
        printf("Existem %d numeros primos entre eles. \n", resultado);
    }
}
