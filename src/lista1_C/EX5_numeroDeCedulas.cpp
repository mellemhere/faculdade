#include <iostream>

/*

    FEITO POR GABRIEL MELLEM
    mellemhere.com
    15.04.2016

 */

int main(int argc, char** argv) {

    /*
        Inicio do programa
     */

    while (1) {
        /*
            (int) valorRecebido = valor que o usuario da ao programa;
            (int) n1	= numero de notas de 1 real;
            (int) n10 = numero de notas de 10 reais;
            (int) n50 = numero de notas de 50 reais;
            (int) valorchegado = soma das notas de 1, 10 e 50;
         */

        //As variaveis estao igualadas as 0 porque cada vez que o programa roda elas devem ser limpas.
        int valorRecebido = 0, valorCompra = 0, n1 = 0, n10 = 0, n50 = 0, valorChegado = 0;

        //Pergunta para o usuario qual o valor em dinheiro
        printf("Valor recebido pelo cliente (R$):");
        //Le o valor e salva em valor recebido
        scanf("%d", &valorRecebido);


        //Pergunta para o usuario qual o valor em dinheiro
        printf("Valor da compra (R$):");
        //Le o valor e salva em valor recebido
        scanf("%d", &valorCompra);
        
        valorRecebido -= valorCompra;

        /*
                EM C A DIVISAO NAO RETORNA RESTO
                Ex. 106 / 50 = 2
         */

        //Tentamos dividir o valor recebido por 50
        if (valorRecebido / 50 > 0) {
            //Caso a divisao de > 0 significa que 'e possivel ter notas de 50
            //Salva quantas notas de 50 'e possivel ter
            n50 = valorRecebido / 50;

            //Salva em valor chegado quanto o programa ja conseguiu se aprocimas do valor dado
            valorChegado = 50 * n50;
        }

        //Tenta executar o mesmo processo anterior, so que agora com 10 
        if (valorRecebido - valorChegado / 10 > 0) {
            n10 = (valorRecebido - valorChegado) / 10;
            valorChegado += 10 * n10;
        }


        //Caso ainda o valor chagado pelo programa nao seja igual ao valor dado
        //complemente com notas de 1
        if (valorChegado != valorRecebido) {
            n1 = valorRecebido - valorChegado;
        }


        //Imprime o resultado
        printf(" Notas de 1: %d \n Notas de 10: %d \n Notas de 50: %d \n", n1, n10, n50);

    }
}