#include <iostream>

/*

    FEITO POR GABRIEL MELLEM
    mellemhere.com
    10.04.2016

 */

int main(int argc, char** argv) {
    //Numero de variaveis que seram trabalhadas
    int NDV = 21;

    //Loop principal do programa
    while (1) {

        //Declara as variaveis para serem usadas usadas
        int contador, numerosPositivo = 0, numerosNegativos = 0;

        /*
            Faz um loop perguntando numeros ate NVD
         */
        for (contador = 1; contador != (NDV + 1); contador++) {
            int numeroDado;
            //Pergunta o numero
            printf("Me de um numero ('%d'): ", contador);
            //Salva o numero em numero dado
            scanf("%d", &numeroDado);

            //Analiza se o numero dado 'e maior que 0
            if (numeroDado > 0) {
                //Se o numero for maior doque 0 adiciona ele em numerosPositivo
                numerosPositivo += numeroDado;
            } else {
                //Caso seja negativo, incremente +1 em numerosNegaticos
                numerosNegativos++;
            }
        }

        //Imprime o resultado
        printf("Soma dos numeros positivos: %d \n", numerosPositivo);
        printf("Quantidade de numeros negativos dados: %d \n", numerosNegativos);
    }
}
