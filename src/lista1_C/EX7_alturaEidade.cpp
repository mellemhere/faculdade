#include <stdio.h>
#include <stdlib.h>
/*
 
    FEITO POR GABRIEL MELLEM
    mellemhere.com
    10.04.2016
 
 */

 
int main(int argc, char** argv) {
    int numeroDeAlunos, cont, alunos15, alunos15Soma, alunosAltura;
	float alunosAlturaSoma;
	
	printf("Quantos alunos seram analizados?");
	scanf("%d", &numeroDeAlunos);
	
	for(cont = 0; cont < numeroDeAlunos;cont++){
		float altura; int idade;
		printf("Qual a altura?");
		scanf("%f", &altura);
		printf("Qual a idade?");
		scanf("%d", &idade);
			
		if(idade > 15){
			alunos15Soma += idade;
			alunos15++;
		}		
		if(altura < 1.70){
			alunosAlturaSoma += altura;
			alunosAltura++;
		}
	}
	
	int mediaIdade, mediaAltura;
	mediaIdade = alunos15Soma / alunos15;
	mediaAltura = alunosAlturaSoma / alunosAltura;
	
	printf("Media da idade de alunos com mais de 15 anos: %d", mediaIdade);
	printf("Media da altura dos alunos com menos de 1.70: %d", mediaAltura);
}

