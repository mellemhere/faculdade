#include <iostream>

/*

    FEITO POR GABRIEL MELLEM
    mellemhere.com
    10.04.2016

 */


int fibo(int n) {
    if (n <= 1) {
        //Caso o valor de n informado for menor ou igual a 1, retorna 1;
        return 1;
    } else {
        /*
         * 	Retorna a soma de todos os valores apartir de n ate chegar a 1;
         * 	Para numeros grandes se torna demorado...
         */
        return fibo(n - 1) + fibo(n - 2);
    }
}

int main(int argc, char** argv) {
    //Define quantas vezes ira calcular o tio fibonacci
    int calcularFiboAte = 11, cont;

    //Faz um loop calulando fibo N vezes definidas por `calcularFiboAte`
    for (cont = 0; cont != calcularFiboAte; cont++) {
        //Calcula fibo
        int number = fibo(cont);
        //Imprime resultado
        printf("%d \n", number);
    }

}
