/*
 * 
 *  FEITO POR GABRIEL MELLEM
 *  10.05.16
 *  
 * 
 * 
 *  SCRIPT CONTA QUANTAS VEZES UMA RODA COM RAIO X GIRA E CALCULA A DISTANCIA
 *  PERCORRIDA.
 * 
 *  TO:DO - Adicionar possibilidade de mais de um sensor de giro.
 *    
 */

//Pin number
int wheel = 2;

//Wheel radius
int wheelRadius = 10; //IN METERS!!!!1

//How many times that we have recived input
int inputTimes = 0;

//Lasttime that the program recived any input
long lastTimeRecived;

void setup() {
    /*
      Start serial for debugging
     */
    Serial.begin(9600);

    lastTimeRecived = millis();
    //  pinMode(wheel, INPUT);
}

void newPass() {
    inputTimes++;
    //updateLDC();
    int x = getDistanceTraveled();
    Serial.print("Distance: ");
    Serial.print(x);
    Serial.print("\n");
}

void loop() {

    int state = analogRead(wheel);
    int forcedState = 0;

    if (state > 1000) {
        if ((millis() - lastTimeRecived) > 2000) {
            lastTimeRecived = millis();
            newPass();
        }
    }
}

int getDistanceTraveled() {
    int c = 2 * 3 * wheelRadius;
    return c * inputTimes;
}