#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
	long long produtoFinal = 1;
	
	int numeroInicial = 92;
	int numeroFinal = 1478;
	
	int contador1 = 0;
	
	/*
		Faz um loop entre os numeros dados
	*/
	for(contador1 = numeroInicial; contador1 != numeroFinal; contador1++){
		
		/*
			Testa se o numero Contador1 'e primo ou nao
		*/
		int contador2;
		int numeroAnalizadoEPrimo = 1; //O numero analizado ja consideramos primo ate que seja provado o contrario
		
		for(contador2 = 2; contador2 < contador1; contador2++){
			if((contador1 % contador2) == 0){
				//Numero nao primo	
				numeroAnalizadoEPrimo = 0;
				break;
			}
		}
		
		if(numeroAnalizadoEPrimo){
			produtoFinal *= contador1;
		}
		
	}
	
	
	printf("%lld", produtoFinal);
	
	return 0;
}