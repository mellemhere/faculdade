package lista1_Java;

import javax.swing.JOptionPane;

public class SegundosParaHMS {

	public static void main(String[] args) {
		/*
		 * 	INICIO DO PROGRAMA
		 */
		
		//Pergunta para o usuario a quantidade de segundos que ele quer converter
		int seconds = Integer.parseInt(JOptionPane.showInputDialog("Informe os segundos:"));
		
		/*
		 *	LOGICA 	
		 */
		
		//Divide as horas por 3600 para obter o numero de horas
		//(Caso o numero for menor que 0 ira ser igual a 0)
		int hours = seconds / 3600;
		//Calcula os minutos com o resto da hora
		//(seconds % 3600) => divide os segundos por 3600 e retorna o resto,
		//que no fim é dividido por 60
		int minutes = (seconds % 3600) / 60;
		
		//Divide o resto dos minutos em segundos 
		int second = seconds % 60;
		
		/*
		 * 	MOSTRA O RESULTADO PARA O USUARIO
		 */
		JOptionPane.showConfirmDialog(null, seconds + " é igual a " 
		+ hours + "h " 
		+ minutes + " min " 
		+ second + " seg");
		
		
		
		
	}
}
