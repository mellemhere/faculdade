/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista1_Java;

import javax.swing.JOptionPane;

/**
 *
 * @author MellemHere
 */
public class CalculadoraSimples {

    public static void main(String[] args) {
        /*
         INICIA CALCULADORA
         */

        //Avisa o usuario que a calculadora ligou
        System.out.println("Calculadora iniciada");

        //Pede pelo primeiro numero
        int first_number = Integer.parseInt(JOptionPane.showInputDialog("Qual o primeiro numero?"));

        //Pede pelo segundo numero
        int second_number = Integer.parseInt(JOptionPane.showInputDialog("Qual o segundo numero?"));

        
        /*
            LOGICA DA CALCULADORA
        */
        //Aqui voce pode descidir oque a calculadora ira fazer
        
        //Adiciona os numeros
        int resultado = add(first_number, second_number);
        
        
        //Subtrai os numeros
        //int resultado = subtract(first_number, second_number);
        
        //Multiplica os numeros
        //int resultado = multiply(first_number, second_number);
        
        
        /*
         MOSTRA O RESULTADO
         */
        JOptionPane.showConfirmDialog(null, "O resultado é: " + resultado);

    }

    /*
     ADICIONA OS DOIS NUMEROS DADOS E RETORNA O RESULTADO
     */
    public static int add(int x, int y) {
        return x + y;
    }

    /*
     SUBTRAI OS DOIS NUMEROS DADOS E RETORNA O RESULTADO
     */
    public static int subtract(int x, int y) {
        return x - y;
    }

    /*
     MULTIPLICA OS DOIS NUMEROS DADOS E RETORNA O RESULTADO
     */
    public static int multiply(int x, int y) {
        return x * y;
    }

}