package lista1_Java;

import javax.swing.JOptionPane;

public class Fibonacci {


	
	public static void main(String[] args) {
		/*
		 * 	INICIA O PROGRAMA PERGUNTANDO EM QUAL N COMEÇAR
		 */
		String start = JOptionPane.showInputDialog("Inicio");
		
		//Define variavel inicio
		int inicio = 1;
		
		//Checa se o usuario inseriu um valor, se sim, deixe o inicio igual ao valor
		//dado pelo usuario
		if(!start.equalsIgnoreCase("")){
			//Deixa o inicio igual ao valor dado pelo usuario.
			inicio = Integer.parseInt(start);
		}
		
		
		//Pergunta ao usuario quantos valores de Fibonacci ele quer
		int max = Integer.parseInt(JOptionPane.showInputDialog("Numero maximo de valores"));
		
		
		/*
		 * 	LOOP PRINCIPAL
		 * 	Executa o calculo d e Fibonacci ate chegar no numero maximo
		 * 	de valores (Deixado)
		 * 	
		 * 	O i é o numero de vezes que o loop foi executado
		 * 	'i != max' => Define que o loop deve executar ate a variavel i for igual ao maximo
		 * 	
		 */
		for(long i = inicio; i != max; i++){
			//Executa a função 'calculateFibonacci' e imprime a resposta.
			System.out.println(calculateFibonacci(i));
		}
		
	}
	
	
	public static long calculateFibonacci(long n){
		if(n <= 1){
			//Caso o valor de n informado for menor ou igual a 1, retorna 1;
			return 1;
		}else{
			/*
			 * 	Retorna a soma de todos os valores apartir de n ate chegar a 1;
			 * 	Para numeros grandes se torna demorado...
			 */
			return calculateFibonacci(n - 1) + calculateFibonacci(n - 2);	
		}
	}
}
