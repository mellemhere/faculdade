package lista1_Java;

import java.util.HashMap;
import javax.swing.JOptionPane;

public class MediaPonderada {
    
    //Variavel para guardar o valor total das notas vezes o valor ponderado
    static Double resultadoNaofinal = 0.0;
    
    //Salva o valor total que ira dividir a nota no final do processo
    static Double dividePor = 0.0;

    public static void main(String[] args) {
        
        
        //Pergunta para o usuario quantas notas serao calculadas
        String nnotas = JOptionPane.showInputDialog("Quantas notas serao verificadas?");

            
        Double numeroDeNotas = Double.parseDouble(nnotas);

        HashMap<Double, Double> notasHolder = new HashMap();

        //Faz um loop perguntando as notas e o valor ponderado
        //Esse loop executa ate a variavel i ser igual ao numeroDeNotas
        for (int i = 0; i != numeroDeNotas; i++) {
            //Pergunta a nota para o usuario
            Double nota = Double.parseDouble(JOptionPane.showInputDialog("Nota: "));
            //Pergunta o valor ponderado pelo usuario
            Double valorPonderado = Double.parseDouble(JOptionPane.showInputDialog("Valor ponderado: "));
            
            //Salva o valor da nota e o valor ponderado para ser processado
            notasHolder.put(nota, valorPonderado);
        }
        
        /*
            Essa parte so é executada depois que o loop termina de perguntar
            para o usuario todas as notas e os valores.
        */
        
        
        //Faz um loop em todas as notas salvas com seus valores ponderados
        notasHolder.forEach((k, v) -> {
            
            //Adiciona em uma variavel o valor da nota vezes o valor ponderado
            resultadoNaofinal += k * v;
            
            //Salva o valor ponderado em uma variavel para usar como divisor depois
            dividePor += v;
        });
        
        
        //Divide o resultadoNaoFinal pelo divisor e mostra para o usuario o resultado final
        JOptionPane.showConfirmDialog(null, "Resultado: " + resultadoNaofinal / dividePor);

    }

}
